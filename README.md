# musicman

> Literally my first node.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve at localhost:8080
$ DEBUG=musicman:* npm start
```
