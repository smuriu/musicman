
exports.up = function(knex, Promise) {
  return knex.schema.createTable('genres', function (table) {
      table.increments('id').primary();
      table.string('name');
  }).createTable('albums', function (table) {
      table.increments('id').primary();
      table.string('title');
  }).createTable('artists', function (table) {
      table.increments('id').primary();
      table.string('name');
  }).createTable('songs', function (table) {
      table.increments('id').primary();
      table.string('title');
      table.integer('genre_id').references('genres.id');
      table.integer('album_id').references('albums.id');
      table.integer('artist_id').references('artists.id');
  });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('songs')
        .dropTable('artists')
        .dropTable('albums')
        .dropTable('genres');
};
