var knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: "./musicman.sqlite"
    }
});

module.exports = require('bookshelf')(knex);
