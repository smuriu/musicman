var bookshelf = require('./bookshelf');

var Genre = bookshelf.Model.extend({
    tableName: 'genres',
    songs: function () {
        return this.hasMany(Song);
    }
});

var Album = bookshelf.Model.extend({
    tableName: 'albums',
    songs: function () {
        return this.hasMany(Song);
    }
})

var Artist = bookshelf.Model.extend({
    tableName: 'artists',
    songs: function () {
        return this.hasMany(Song);
    }
});

var Song = bookshelf.Model.extend({
    tableName: 'songs',
    artist: function () {
        return this.belongsTo(Artist);
    },
    genre: function () {
        return this.belongsTo(Genre);
    },
    album: function () {
        return this.belongsTo(Album);
    }
});

module.exports = {
    Genre,
    Album,
    Artist,
    Song
}
