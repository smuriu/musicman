var express = require('express');
var router = express.Router();

var Album = require('../models').Album;

router.route('/')
    // List albums
    .get(function (req, res) {
        Album.collection().fetch().then(function (collection) {
            res.send(collection.toJSON());
        });
    })
    // Add an album
    .post(function (req, res) {
        Album.forge()
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    });

router.route('/:id')
    // Fetch an album by id
    .get(function (req, res) {
        Album.forge({ id: req.params.id })
            .fetch()
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Update an album
    .put(function (req, res) {
        Album.forge({ id: req.params.id })
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Delete an album
    .delete(function (req, res) {
        Album.forge({ id: req.params.id })
            .destroy()
            .then(function () {
                res.status(204).end();
            });
    });

module.exports = router;
