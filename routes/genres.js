var express = require('express');
var router = express.Router();

var Genre = require('../models').Genre;

router.route('/')
    // List genres
    .get(function (req, res) {
        Genre.collection().fetch().then(function (collection) {
            res.send(collection.toJSON());
        });
    })
    // Add a genre
    .post(function (req, res) {
        Genre.forge()
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    });

router.route('/:id')
    // Fetch a genre by id
    .get(function (req, res) {
        Genre.forge({ id: req.params.id })
            .fetch()
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Update a genre
    .put(function (req, res) {
        Genre.forge({ id: req.params.id })
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Delete a genre
    .delete(function (req, res) {
        Genre.forge({ id: req.params.id })
            .destroy()
            .then(function () {
                res.status(204).end();
            });
    });

module.exports = router;
