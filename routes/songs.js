var express = require('express');
var router = express.Router();

var Song = require('../models').Song;

router.route('/')
    // List songs
    .get(function (req, res) {
        Song.collection().fetch().then(function (collection) {
            res.send(collection.toJSON());
        });
    })
    // Add a song
    .post(function (req, res) {
        Song.forge()
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    });

router.route('/:id')
    // Fetch a song by id
    .get(function (req, res) {
        Song.forge({id: req.params.id})
            .fetch()
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Update a song
    .put(function (req, res) {
        Song.forge({ id: req.params.id })
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Delete a song
    .delete(function (req, res) {
        Song.forge({id: req.params.id})
            .destroy()
            .then(function () {
                res.status(204).end();
            });
    });

module.exports = router;
