var express = require('express');
var router = express.Router();

var Artist = require('../models').Artist;

router.route('/')
    // List artists
    .get(function (req, res) {
        Artist.collection().fetch().then(function (collection) {
            res.send(collection.toJSON());
        });
    })
    // Add an artist
    .post(function (req, res) {
        Artist.forge()
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    });

router.route('/:id')
    // Fetch an artist by id
    .get(function (req, res) {
        Artist.forge({ id: req.params.id })
            .fetch()
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Update an artist
    .post(function (req, res) {
        Artist.forge({ id: req.params.id })
            .save(req.body)
            .then(function (model) {
                res.send(model.toJSON());
            });
    })
    // Delete an artist
    .delete(function (req, res) {
        Artist.forge({ id: req.params.id })
            .destroy()
            .then(function () {
                res.status(204).end();
            });
    });

module.exports = router;
